﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Pacman
{
    public partial class GameWindow : Form
    {
        //counter to determine Framse Per Second.
        private int FramesCount = 0;

        private int _lives = 3;
        private int _score = 0;

        private int pelletsRemaining = 0;

        //Graphics buffer variables.
        BufferedGraphicsContext currentContext;
        BufferedGraphics CanvasBuffer;
        //To hold the game background/grid
        Bitmap GameGridDefaultGraphics;

        /// <summary>
        /// Draws the game background/grid onto the provided canvas.
        /// </summary>
        /// <param name="canvas"></param>
        private void DrawGameGrid(Graphics canvas)
        {
            //the canvas is defined to be 700x700px, so we can assume that each grid square is 28x28px

            //draw lines from top to bottom
            for (int i = 0; i < 25; i++)
                for (int j = 0; j < 25; j++)
                {
                    canvas.DrawImage(GameObject.Variables.Grid[i, j].TileImage, i * 28, j * 28);
                }
        }

        /// <summary>
        /// Iterate thought the sprites, drawing each one.
        /// </summary>
        /// <param name="canvas"></param>
        private void DrawSprites(BufferedGraphics canvas)
        {
            foreach (GameObject.Sprite S in GameObject.Variables.Sprites)
            {
                canvas.Graphics.DrawImage(S.SpriteImage, S.verticies[0]);
            }
        }

        /// <summary>
        /// Initialisation, simply sets up the graphics buffer.
        /// </summary>
        public GameWindow()
        {
            InitializeComponent();

            currentContext = BufferedGraphicsManager.Current;
            CanvasBuffer = currentContext.Allocate(GameScreen.CreateGraphics(), GameScreen.DisplayRectangle);
        }

        private void InitialiseGameState()
        {
            //ensure that all gamestate properties are refreshed
            //Sprites
            GameObject.Variables.Sprites = new List<GameObject.Sprite>();
            //Tiles
            GameObject.Variables.Grid = new GameObject.Tile[25, 25];

            _lives = 3;
            _score = 0;


            //populate the array of tiles
            for (int i = 0; i < 25; i++)
                for (int j = 0; j < 25; j++)
                    GameObject.Variables.Grid[i, j] = new GameObject.Tile();

            //load the level design from the file
            List<String> fileContents = new List<String>();

            //read the file into the list of strings
            using (StreamReader reader = new StreamReader("./LevelLayout.csv"))
            {
                while (!reader.EndOfStream)
                {
                    fileContents.AddRange(reader.ReadLine().Split(','));
                }
            }

            for (int i = 0; i < 25 * 25; i++)
            {
                int type = 0;

                if (!int.TryParse(fileContents[i], out type)) type = 0;
                {
                    GameObject.Variables.Grid[i % 25, i / 25].type = (GameObject.Tile.TileType)type;
                    GameObject.Variables.Grid[i % 25, i / 25].TileConsumed += HandleTileConsumed;

                    if(GameObject.Variables.Grid[i % 25, i / 25].type == GameObject.Tile.TileType.Pellet || GameObject.Variables.Grid[i % 25, i / 25].type == GameObject.Tile.TileType.PowerPellet)
                    {
                        pelletsRemaining++;
                    }
                }
            }

            //add a player to the game
            GameObject.Variables.Sprites.Add(new GameObject.Player());
            GameObject.Variables.Sprites.Add(new GameObject.Blinky());
            GameObject.Variables.Sprites.Add(new GameObject.Inky());
            GameObject.Variables.Sprites.Add(new GameObject.Pinky());
            GameObject.Variables.Sprites.Add(new GameObject.Clyde());

            foreach (GameObject.Sprite S in GameObject.Variables.Sprites)
            {
                if (S is GameObject.Player)
                    S.SpriteDied += HandlePlayerDied;
                if (S is GameObject.Ghost)
                    S.SpriteDied += HandleGhostDied;
            }

            //the canvas is defined to be 700x700px, so we can assume that each grid square is 28x28px
            GameGridDefaultGraphics = new Bitmap(700, 700);
            Graphics Gobj = Graphics.FromImage(GameGridDefaultGraphics);
            //draw lines from top to bottom
            for (int i = 0; i < 25; i++)
                for (int j = 0; j < 25; j++)
                {
                    Gobj.DrawImage(GameObject.Variables.Grid[i, j].TileImage, i * 28, j * 28);
                }
            Gobj.Dispose();
        }

        private void HandlePlayerDied(object Sender, EventArgs e)
        {
            _lives--;
            _score -= 4000;

            //if the player is out of lives, then end the game
            if (_lives == 0)
                StopGameThread();
            //if the player has lives remaining, reset the sprites to initial positions and states.
            else
            {
                foreach (GameObject.Sprite S in GameObject.Variables.Sprites)
                {
                    S.Reset();
                }
            }
        }

        private void HandleGhostDied(Object Sender, EventArgs e)
        {
                _score += 2000;
        }

        private void HandleTileConsumed(Object Sender, EventArgs e)
        {
            //50 points for consuming a pellet
            if(((GameObject.Tile)Sender).type == GameObject.Tile.TileType.Pellet)
            {
                _score += 100;
                pelletsRemaining--;
            }

            //100 points for consuming a power pellet
            if(((GameObject.Tile)Sender).type == GameObject.Tile.TileType.PowerPellet)
            {
                _score += 200;
                pelletsRemaining--;
                foreach(GameObject.Sprite S in GameObject.Variables.Sprites)
                {
                    if(S is GameObject.Ghost)
                    {
                        ((GameObject.Ghost)(S)).vulnerable = true;
                    }
                }
            }
        }


        System.Diagnostics.Stopwatch GameClock = new System.Diagnostics.Stopwatch();
        /// <summary>
        /// The game-loop is the primrary function of the game
        /// This should be run in a thread to prevent locking up the main form process.
        /// Also, this regularly paints to the panel, and writes to the FPS counter, so those
        ///     variables should not be written by the main thread.
        /// </summary>
        private void GameLoop()
        {
            //Game loop
            bool gameInProgress = true;

            //Set up the background, reset all sprites, etc.
            InitialiseGameState();

            
            GameClock.Start();


            while (pelletsRemaining > 0)
            {
                //Calculate the time at which the next frame should tirgger
                //recalculated next time to account for missed deadlines.
                GameClock.Restart();
                System.Diagnostics.Debug.Print("Frame Start");
                //add to the FPS counter
                FramesCount++;
                //Put the default background layout on the grid
                //CanvasBuffer.Graphics.DrawImage(GameGridDefaultGraphics, 0, 0);
                DrawGameGrid(CanvasBuffer.Graphics);
                System.Diagnostics.Debug.Print("Draw Background");
                System.Diagnostics.Debug.Print(((double)GameClock.ElapsedTicks / (double)System.Diagnostics.Stopwatch.Frequency).ToString());

                //Execute a tick for each sprite
                foreach (GameObject.Sprite S in GameObject.Variables.Sprites)
                    S.tick();


                System.Diagnostics.Debug.Print("Tick Sprites");
                System.Diagnostics.Debug.Print(((double)GameClock.ElapsedTicks / (double)System.Diagnostics.Stopwatch.Frequency).ToString());
                //Draw the sprites
                DrawSprites(CanvasBuffer);
                System.Diagnostics.Debug.Print("Draw Sprties");
                System.Diagnostics.Debug.Print(((double)GameClock.ElapsedTicks / (double)System.Diagnostics.Stopwatch.Frequency).ToString());

                //Render the frame on screen
                CanvasBuffer.Render();
                System.Diagnostics.Debug.Print("Render");
                System.Diagnostics.Debug.Print(((double)GameClock.ElapsedTicks / (double)System.Diagnostics.Stopwatch.Frequency).ToString());

                _score--;
                //wait until end of frame time to start next frame
                while (GameClock.ElapsedTicks < System.Diagnostics.Stopwatch.Frequency/60) ;
            }

            GameClock.Stop();
            UITimer.Stop();
            GameThreadRunning = false;
        }

        /// <summary>
        /// Terminates the game thread, ensuring that the game ends cleanly.
        /// </summary>
        private void StopGameThread()
        {
            GameClock.Stop();
            UITimer.Stop();
            GameThreadRunning = false;
            gameThread.Abort();
        }

        /// <summary>
        /// Timer that measures the frame rate of the application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FPSCount_Tick(object sender, EventArgs e)
        {
            if (GameThreadRunning)
            {
                FPS.Text = (FramesCount).ToString();
                FramesCount = 0;
            }
            else
            {
                FPS.Text = "FPS";
                FramesCount = 0;
            }
        }

        private bool GameThreadRunning = false;
        private Thread gameThread;
        /// <summary>
        /// On click anywhere on the window or image frame, trigger a game start.
        /// Will not start the game if it is already running.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GameWindow_Click(object sender, EventArgs e)
        {
            if (GameThreadRunning == false)
            {
                GameThreadRunning = true;
                //Start the game timer disabled due to resolution
                //FrameTimer.Start();
                FPSCount.Start();
                UITimer.Start();

                //execute game loop
                gameThread = new Thread(GameLoop);
                gameThread.Start();
            }
        }

        /// <summary>
        /// This key-preview event manages the user input.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GameWindow_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            //only use if the game thread is running.
            //WASD = Movement
            //ESC = pause
            if(GameThreadRunning)
                switch(e.KeyCode)
                {
                    case Keys.W:
                        foreach (GameObject.Sprite S in GameObject.Variables.Sprites)
                            if (S is GameObject.Player)
                                ((GameObject.Player)S).Facing = GameObject.Sprite.Direction.Up;
                        break;
                    case Keys.A:
                        foreach (GameObject.Sprite S in GameObject.Variables.Sprites)
                            if (S is GameObject.Player)
                                ((GameObject.Player)S).Facing = GameObject.Sprite.Direction.Left;
                        break;
                    case Keys.S:
                        foreach (GameObject.Sprite S in GameObject.Variables.Sprites)
                            if (S is GameObject.Player)
                                ((GameObject.Player)S).Facing = GameObject.Sprite.Direction.Down;
                        break;
                    case Keys.D:
                        foreach (GameObject.Sprite S in GameObject.Variables.Sprites)
                            if(S is GameObject.Player)
                                ((GameObject.Player)S).Facing = GameObject.Sprite.Direction.Right;
                        break;
                    case Keys.Escape:
                        StopGameThread();
                        break;

                }
        }

        /// <summary>
        /// Clean-up for when the game window is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GameWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopGameThread();
        }

        private void UITimer_Tick(object sender, EventArgs e)
        {
            Lives.Text = _lives.ToString();
            Score.Text = _score.ToString();
        }
    }
}

/// <summary>
/// Namespace for all game objects (Pacman, Ghosts, Walls, Pellets
/// </summary>
namespace GameObject
{
    public static class Variables
    {
        public static Random RNG = new Random();
        //A grid of Tile objects, which houses the game board
        public static Tile[,] Grid;
        //List of all sprites (PC and NPC).
        public static List<Sprite> Sprites = new List<GameObject.Sprite>();

        /// <summary>
        /// Determines if the given pixel contains a wall
        /// </summary>
        /// <param name="p"></param>
        /// <returns>False if Grid is not initialised. Otherwise whether the indicated pixel is in a wall tile.</returns>
        public static Tile.TileType whichTile(Point p)
        {
            //no grid, nonsence query, assume that nothing is a wall.
            if (Grid == null) return Tile.TileType.Wall;

            int x = p.X / 28;
            int y = p.Y / 28;

            return Grid[x, y].type;
        }

        public static Point SpriteTile<T>()
        {
            foreach (GameObject.Sprite S in GameObject.Variables.Sprites)
                if (S is T)
                    return S.findCurrentTile();

            return new Point(0, 0);
        }


        /// <summary>
        /// Detect is the specified sprite collides with any other sprite in the Sprites list
        /// </summary>
        /// <param name="S"></param>
        /// <returns>A list of all sprites that the specified sprite collides with</returns>
        public static List<Sprite> SpriteCollision(Sprite S)
        {
            List<Sprite> returnList = new List<Sprite>();
            //check for collisions with other sprites
            foreach(Sprite Other in Sprites)
            {
                if(Other != S)
                {
                    Point Scentre = Point.Add(S.verticies[(int)Sprite.Corner.TL], new Size(14, 14));
                    Point Ocentre = Point.Add(Other.verticies[(int)Sprite.Corner.TL], new Size(14, 14));
                    if(Math.Sqrt((double)(Math.Pow(Scentre.X-Ocentre.X,2)+Math.Pow(Scentre.Y-Ocentre.Y,2)))<=28)
                    {
                        returnList.Add(Other);
                    }                        
                }
            }

            return returnList;
        }
    }


    public class Sprite
    {
        /// <summary>
        /// Conveniant way to discuss direction
        /// </summary>
        public enum Direction
        {
            Up = 0,
            Right = 1,
            Down = 2,
            Left = 3
        }

        //private interface to the sprite direction
        protected Direction _Facing;


        /// <summary>
        /// Public interface to the sprite direction
        /// Read and write
        /// </summary>
        public Direction Facing
        {
            get { return _Facing; }
        }


        /// <summary>
        /// The position of the top left pixel of the character
        /// </summary>
        protected Point TL_pos;

        public enum Corner
        {
            TL = 0,
            TR = 1,
            BR = 2,
            BL = 3
        }

        /// <summary>
        /// The verticies of the sprite, in form [TL,TR,BR,BL]
        /// </summary>
        public Point[] verticies
            {
                get
                {
                    Point[] returnVal = { TL_pos,
                        new Point(TL_pos.X + 27, TL_pos.Y),
                        new Point(TL_pos.X + 27, TL_pos.Y + 27),
                        new Point(TL_pos.X, TL_pos.Y + 27) };
                    return returnVal;
                }
            }


        // Countdown variable to next move event
        protected int _tickCount = 0;
        
        // Delay in frames between move events
        protected int move_delay = 0;
        /// <summary>
        /// Public interface to the number of ticks remaining before movement is possible.
        /// Read only.
        /// </summary>
        public int tickCount
        {
            get { return _tickCount; }
        }

        /// <summary>
        /// Private variable to store the bitmap, can be modified only by this object
        /// </summary>
        protected Bitmap _SpriteImage;

        /// <summary>
        /// Public interface to the sprite bitmap, read-only.
        /// </summary>

        public virtual Bitmap SpriteImage
        {
            get { return _SpriteImage; }
        }

        public Point findCurrentTile()
        {
            Point Tile = Point.Add(TL_pos, new Size(14, 14));
            Tile.X /= 28;
            Tile.Y /= 28;

            return Tile;
        }


        protected virtual void Constructor(Point InitialPosition, Brush FillColor)
        {
            TL_pos = InitialPosition;

            //need sprite image
            _SpriteImage = new Bitmap(27, 27);
            Graphics Gobj = Graphics.FromImage(_SpriteImage);
            Gobj.FillEllipse(FillColor, 0, 0, 27, 27);
            Gobj.Dispose();
        }

        protected virtual bool WalkableTile(Tile.TileType query)
        {
            //base class assumes that you can only walk on path
            return (query == Tile.TileType.Path || query == Tile.TileType.Pellet || query == Tile.TileType.PowerPellet);
        }

        protected virtual bool CanMove(Direction d)
        {
            Point interest;
            switch(d)
            {
                case Direction.Up:
                    //check above top-left.
                    interest = verticies[(int)Corner.TL];
                    interest.Y -= 2;
                    if (!WalkableTile(Variables.whichTile(interest))) return false;
                    //check above top-right
                    interest = verticies[(int)Corner.TR];
                    interest.Y -= 2;
                    if (!WalkableTile(Variables.whichTile(interest))) return false;
                    break;
                case Direction.Down:
                    //check below bottom-left.
                    interest = verticies[(int)Corner.BL];
                    interest.Y += 2;
                    if (!WalkableTile(Variables.whichTile(interest))) return false;
                    //check below bottom-right
                    interest = verticies[(int)Corner.BR];
                    interest.Y += 2;
                    if (!WalkableTile(Variables.whichTile(interest))) return false;
                    break;
                case Direction.Left:
                    //check left of top-left
                    interest = verticies[(int)Corner.TL];
                    interest.X -= 2;
                    if (!WalkableTile(Variables.whichTile(interest))) return false;
                    //check left of bottom-left
                    interest = verticies[(int)Corner.BL];
                    interest.X -= 2;
                    if (!WalkableTile(Variables.whichTile(interest))) return false;
                    break;
                case Direction.Right:
                    //check right of top-right
                    interest = verticies[(int)Corner.TR];
                    interest.X += 2;
                    if (!WalkableTile(Variables.whichTile(interest))) return false;
                    //check right of bottom-right
                    interest = verticies[(int)Corner.BR];
                    interest.X += 2;
                    if (!WalkableTile(Variables.whichTile(interest))) return false;
                    break;
            }

            return true;
        }

        /// <summary>
        /// Internal method, which moves the character according to its type.
        /// </summary>
        protected virtual void move()
        {
            //perform movement.
            if(CanMove(_Facing))
                switch (_Facing)
                {
                    case Direction.Up:
                        TL_pos.Y -= 2;
                        //Screen-edge teleport
                        if (TL_pos.Y < 15)
                            TL_pos.Y += 644;
                        break;
                    case Direction.Right:
                        TL_pos.X += 2;
                        //Screen-edge teleport
                        if (TL_pos.X > 661)
                            TL_pos.X -= 644;
                        break;
                    case Direction.Down:
                        TL_pos.Y += 2;
                        //Screen-edge teleport
                        if (TL_pos.Y > 661)
                            TL_pos.Y -= 644;
                        break;
                    case Direction.Left:
                        TL_pos.X -= 2;
                        //Screen-edge teleport
                        if (TL_pos.X < 15)
                            TL_pos.X += 644;
                        break;
                }
        }

        /// <summary>
        /// 
        /// What to do on each game tick
        /// Decrements the tick-counter
        /// If tick-counter is zero
        ///     perform frame action
        ///     reset tick counter.
        /// </summary>
        /// <returns>True if player is dead</returns>
        public virtual void tick()
        {
            if (_tickCount == 0)
            {
                _tickCount = move_delay;
                move();
            }
            else _tickCount -= 1;
        }

        /// <summary>
        /// Performs collision processing
        /// </summary>
        public virtual void Collide(Sprite Caller)
        {
            //action for collision
        }

        public event EventHandler SpriteDied;

        /// <summary>
        /// Kills the sprite.
        /// </summary>
        public virtual void Die()
        {
            SpriteDied?.Invoke(this, new EventArgs());
            Reset();
        }

        /// <summary>
        /// Resets the sprite to initial conditions (class defaults).
        /// </summary>
        public virtual void Reset()
        {
            //reset logic
        }
    }

    public class Player : Sprite
    {
        //need a thnig for new_facing, as the player determines the direction, and can pre-load
        protected Direction _newFacing;

        new public Direction Facing
        {
            get { return base.Facing; }
            set { _newFacing = value; }
        }



        public Player(Point InitialPosition)
        {
            base.Constructor(InitialPosition, Brushes.Yellow);
        }

        public Player()
        {
            Reset();
        }

        override protected void move()
        {
            if (CanMove(_newFacing)) _Facing = _newFacing;
            base.move();

            //consume the current tile, to remove the dot.
            Point Position = findCurrentTile();
            Variables.Grid[Position.X, Position.Y].Consume();
        }

        public override void Die()
        {
            //kill the character
            base.Die();
        }

        public override void Reset()
        {
            base.Constructor(new Point(12 * 28, 10 * 28), Brushes.Yellow);
            _Facing = Direction.Up;
            _newFacing = Direction.Up;
        }

        public override void tick()
        {
            base.tick();

            //find all sprites that collide with Pacman
            List<Sprite> Collisions = Variables.SpriteCollision(this);

            //if the list is not empty, then we have a collision.
            if(Collisions.Count > 0)
            {
                foreach (Sprite s in Collisions)
                {
                    //get the sprite to perform its colission check.
                    s.Collide(this);
                }
            }
        }
    }

    public class Ghost : Sprite
    {
        protected Point _Home;

        protected int ModeCount = 1800;

        protected Mode CurrentMode = Mode.Chase;

        private Bitmap _scaredImage;

        public override Bitmap SpriteImage
        {
            get
            {
                if (vulnerable)
                {
                    return _scaredImage;
                }
                else
                    return base.SpriteImage;
            }
        }

        protected enum Mode
        {
            Chase = 0,
            Home = 1
        }

        //Need a setting for ghosts in vulnerable mode
        protected bool _vulnerable;
        public bool vulnerable
        {
            get { return _vulnerable; }
            set
            {
                _vulnerable = value;
                if (value == true)
                    vulnerable_ticks = 600;
                else
                    vulnerable_ticks = 0;
            }
        }

        protected int vulnerable_ticks;

        public Ghost()
        {
            Reset();
        }

        public override void Reset()
        {
            Constructor(new Point(12 * 28, 12 * 28), Brushes.Green, new Point(12 * 28, 12 * 28));
        }

        protected void Constructor(Point Start, Brush Color, Point Home)
        {
            base.Constructor(Start, Color);
            //ghosts move slower than PacMan
            move_delay = 1;
            _Home = Home;

            _scaredImage = new Bitmap(27, 27);
            Graphics Gobj = Graphics.FromImage(_scaredImage);
            Gobj.FillEllipse(Brushes.DarkBlue, 0, 0, 27, 27);
            Gobj.Dispose();
        }

        override protected bool CanMove(Direction d)
        {
            if (d == Direction.Up)
            {
                //override the up direction check to allow for movement through ghost-walls.
                //check above top-left.
                Point interest = verticies[(int)Corner.TL];
                interest.Y -= 2;
                if (Variables.whichTile(interest) == Tile.TileType.GhostWall) return true;
                //check above top-right
                interest = verticies[(int)Corner.TR];
                interest.Y -= 2;
                if (Variables.whichTile(interest) == Tile.TileType.GhostWall) return true;
            }

            return base.CanMove(d);
        }

        protected virtual Point SelectTargetTile()
        {
            Point Target = new Point(0, 0);

            Point pacman = Variables.SpriteTile<Player>();
            //ghost specific direction choosing code
            //http://gameinternals.com/post/2072558330/understanding-pac-man-ghost-behavior


            //random target
            Target.X = Variables.RNG.Next(1, 24);
            Target.Y = Variables.RNG.Next(1, 24);

            return Target;
        }

        protected Direction SelectFacing()
        {
            //first find which square we are currently in
            Point Current = findCurrentTile();

            //there are some special tiles which can trap the ghosts.
            //if the ghost is in the jail tiles, then we need to hard-code their escape.
            if ((Current.Y == 11 || Current.Y == 12) && Current.X == 12)
                return Direction.Up;

            if (Current.Y == 12)
            {
                if (Current.X == 11)
                    return Direction.Right;
                if (Current.X == 13)
                    return Direction.Left;
            }

            //if the sprite is in a position where it can select a direction
            //run algorithm to determine new direction.
            //select the target square
            Point Target = SelectTargetTile();

            //if in home mode, then modify the target location.
            if (CurrentMode == Mode.Home)
                Target = _Home;

            //random target when vulnerable, takes precedence over all other behaviour
            if (vulnerable)
            {
                Target.X = Variables.RNG.Next(1, 24);
                Target.Y = Variables.RNG.Next(1, 24);
            }

            //now look at which directions we can move in.
            //Ghosts cannot turn 180 degrees.
            bool[] ValidMoves = { true, true, true, true };
            Point[] NewPositions = { new Point(-1, -1), new Point(-1, -1), new Point(-1, -1), new Point(-1, -1) };
            double[] vectorLengths = { double.MaxValue, double.MaxValue, double.MaxValue, double.MaxValue };

            //by putting the move directions in their defined order, we can do some fast array manipulation
            ValidMoves[((int)Facing + 2) % 4] = false;

            //now eliminate directions that we cannot move in.
            if (!CanMove(Direction.Up))
                ValidMoves[(int)Direction.Up] = false;
            if (!CanMove(Direction.Right))
                ValidMoves[(int)Direction.Right] = false;
            if (!CanMove(Direction.Down))
                ValidMoves[(int)Direction.Down] = false;
            if (!CanMove(Direction.Left))
                ValidMoves[(int)Direction.Left] = false;

            //we are left with an array which holds true only in valid move directions.
            //now find which squares we sould be in after moving in that direction.

            if (ValidMoves[(int)Direction.Down])
                NewPositions[(int)Direction.Down] = Point.Add(Current, new Size(0, 1));
            if (ValidMoves[(int)Direction.Up])
                NewPositions[(int)Direction.Up] = Point.Add(Current, new Size(0, -1));
            if (ValidMoves[(int)Direction.Right])
                NewPositions[(int)Direction.Right] = Point.Add(Current, new Size(1, 0));
            if (ValidMoves[(int)Direction.Left])
                NewPositions[(int)Direction.Left] = Point.Add(Current, new Size(-1, 0));

            //now determine vector length to target from each new point
            for (int i = 0; i < 4; i++)
            {
                if (ValidMoves[i])
                {
                    //we can re-use current for this, as it is no longer needed
                    Current = Point.Subtract(Target, (Size)NewPositions[i]);
                    vectorLengths[i] = Math.Sqrt(Math.Pow(Current.X, 2) + Math.Pow(Current.Y, 2));
                }
            }

            Direction BestChoice = (Direction)0;
            //now find the direction with the shortest direction length. That is the direction we want to move in.
            for (int i = 1; i < 4; i++)
            {
                if (vectorLengths[i] < vectorLengths[(int)BestChoice] && ValidMoves[i])
                    BestChoice = (Direction)i;
            }

            return BestChoice;
        }

        protected override void move()
        {
            //only change facing, if completely on a tile
            if (TL_pos.X % 28 == 0 && TL_pos.Y %28 == 0)
                _Facing = SelectFacing();

            //move like anything else.
            base.move();
        }

        public override void Collide(Sprite S)
        {
            //if the ghost is vulnerable, then pacman kills it.
            if(vulnerable)
                Die();
            //if the ghost is not vulnerable, then it kills pacman.
            else
                S.Die();
        }

        override public void tick()
        {
            if (_vulnerable)
            {
                vulnerable_ticks--;
                if (vulnerable_ticks <= 0)
                {
                    _vulnerable = false;
                }
            }
            //ensure that this happens on the frame that ghosts become invincible again
            if(!_vulnerable)
            {
                ModeCount--;
                if (ModeCount <= 0)
                {
                    //every so often, the ghosts run to their corners.
                    switch (CurrentMode)
                    {
                        case Mode.Chase:
                            CurrentMode = Mode.Home;
                            ModeCount = 600;
                            //move faster on the way home
                            move_delay = 0;
                            break;
                        case Mode.Home:
                            CurrentMode = Mode.Chase;
                            ModeCount = 1800;
                            //move slower when chasing
                            move_delay = 1;
                            break;
                    }
                    //change direction to notify of behavior change
                    _Facing = (Direction)(((int)_Facing + 2) % 4);
                }
            }
            base.tick();
        }
    }

    public class Blinky : Ghost
    {

        override protected Point SelectTargetTile()
        {
            //ghost specific direction choosing code
            //http://gameinternals.com/post/2072558330/understanding-pac-man-ghost-behavior            

            return Variables.SpriteTile<Player>();
        }

        public Blinky()
        {
            Reset();
        }

        public override void Reset()
        {
            //release blinky after half a second
            _tickCount = 30 + vulnerable_ticks;
            base.Constructor(new Point(12 * 28, 12 * 28), Brushes.Red, new Point(22, 22));
        }

    }

    public class Pinky : Ghost
    {

        override protected Point SelectTargetTile()
        {
            //ghost specific direction choosing code
            //http://gameinternals.com/post/2072558330/understanding-pac-man-ghost-behavior     



            //find pacmans direction
            Direction PacmanDirection = Direction.Up;

            foreach (Sprite S in Variables.Sprites)
                if (S is Player)
                    PacmanDirection = S.Facing;

            //Target:4-forward of PacMan
            Point Target = Variables.SpriteTile<Player>();
            switch (PacmanDirection)
            {
                case Direction.Up:
                    Target.Y -= 4;
                    break;
                case Direction.Down:
                    Target.Y += 4;
                    break;
                case Direction.Left:
                    Target.X -= 4;
                    break;
                case Direction.Right:
                    Target.X += 4;
                    break;
            }
            return Target;
        }

        public Pinky()
        {
            Reset();
        }

        public override void Reset()
        {
            //release pinky after two seconds
            _tickCount = 120 + vulnerable_ticks;
            base.Constructor(new Point(12 * 28, 12 * 28), Brushes.Pink, new Point(2, 2));
        }
    }

    public class Inky : Ghost
    {
        override protected Point SelectTargetTile()
        {
            //ghost specific direction choosing code
            //http://gameinternals.com/post/2072558330/understanding-pac-man-ghost-behavior   

            Point Target = new Point(10, 12);

            //find pacmans direction
            Direction PacmanDirection = Direction.Up;

            foreach (Sprite S in Variables.Sprites)
                if (S is Player)
                    PacmanDirection = S.Facing;

            //find pacmans location
            Point PacmanLocation = Variables.SpriteTile<Player>();
            //target: directly across from Blinky (through four tiles in front of pac-man)
            Target = PacmanLocation;
            switch (PacmanDirection)
            {
                case Direction.Up:
                    Target.Y -= 4;
                    break;
                case Direction.Down:
                    Target.Y += 4;
                    break;
                case Direction.Left:
                    Target.X -= 4;
                    break;
                case Direction.Right:
                    Target.X += 4;
                    break;
            }
            //Find blinkys position
            Point BlinkyPos = Variables.SpriteTile<Blinky>();
            Point vector = Point.Subtract(Target, (Size)BlinkyPos);
            Target = Point.Add(Target, (Size)vector);

            return Target;
        }

        public Inky()
        {
            Reset();
        }

        public override void Reset()
        {
            //release inky after four seconds
            _tickCount = 240 + vulnerable_ticks;
            base.Constructor(new Point(12 * 28, 12 * 28), Brushes.Cyan, new Point(2, 22));
        }
    }

    public class Clyde : Ghost
    {

        override protected Point SelectTargetTile()
        {
            return base.SelectTargetTile();
        }

        public Clyde()
        {
            Reset();
        }

        public override void Reset()
        {
            //release clyde after eight seconds
            _tickCount = 480 + vulnerable_ticks;
            base.Constructor(new Point(12 * 28, 12 * 28), Brushes.Orange, new Point(22, 2));
        }

    }

    /// <summary>
    /// A tile is a 28px by 28 px square on the grid
    /// There are 25x25=125 tiles in the grid
    /// </summary>
    public class Tile
    {
        /// <summary>
        /// Convienient way to see what type a tile is.
        /// A path is a valid tile to walk on
        /// A wall is a invalid tile to walk on
        /// A ghost-wall is a tile that is valid for only ghosts to walk on.
        /// </summary>
        public enum TileType
        {
            Path=0,
            Wall=1,
            GhostWall=2,
            Pellet = 3,
            PowerPellet = 4
        }

        /// <summary>
        /// Private interface to the tile image, read/write
        /// </summary>
        private Bitmap _TileImage;
        /// <summary>
        /// Public interface to the tile image
        /// Read Only.
        /// </summary>
        public Bitmap TileImage
        {
            get
            {
                return _TileImage;
            }
        }

        /// <summary>
        /// Private interface to the tile type, read/write
        /// </summary>
        private TileType _type;
        /// <summary>
        /// Public intercace to the tile type
        /// Read/write
        /// On read, Re-initialises the object, as the type has changed and the object has new properties.
        /// </summary>
        public TileType type
        {
            set
            {
                Constructor(value);
            }
            get
            {
                return _type;
            }
        }

        /// <summary>
        /// Public constructor for the tile
        /// </summary>
        /// <param name="type">The type of tile (wall/sprite/etc)</param>
        public Tile(TileType type)
        {
            Constructor(type);
        }

        /// <summary>
        /// Public constructor for the tile
        /// Defaults to Path tile.
        /// </summary>
        public Tile()
        {
            Constructor(TileType.Path);
        }

        /// <summary>
        /// Trigger to comsume the current tile, converting it to a path tile.
        /// </summary>
        public void Consume()
        {
            TileConsumed?.Invoke(this, new EventArgs());
            Constructor(TileType.Path);
        }

        /// <summary>
        /// Event raised when the tile is consumed. Provides the type of tile which was consumed
        /// </summary>
        public event EventHandler TileConsumed;

        /// <summary>
        /// Private internal constructer, which is called by all public constructors.
        /// </summary>
        /// <param name="type">The type of tile (wall/path/etc)</param>
        private void Constructor(TileType type)
        {
            _type = type;
            _TileImage = new Bitmap(28, 28);
            Graphics Gobj = Graphics.FromImage(_TileImage);

            //walls are black tiles, path is white tiles.
            switch (type)
            {
                case TileType.Wall:
                    Gobj.FillRectangle(Brushes.Black, 0, 0, 28, 28);
                    break;
                case TileType.GhostWall:
                    Gobj.FillRectangle(Brushes.LightGray, 0, 0, 28, 28);
                    break;
                case TileType.Pellet:
                    Gobj.FillRectangle(Brushes.White, 0, 0, 28, 28);
                    Gobj.FillEllipse(Brushes.DimGray, 9, 9, 5, 5);
                    break;
                case TileType.PowerPellet:
                    Gobj.FillRectangle(Brushes.White, 0, 0, 28, 28);
                    Gobj.FillEllipse(Brushes.DimGray, 4,4,10,10);
                    break;
                default:
                    Gobj.FillRectangle(Brushes.White, 0, 0, 28, 28);
                    break;
            }
            Gobj.Dispose();
        }
    }
}